﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using ChatApplication2.Models.ChatModels;
using ChatApplication2.Models.DTO;
using Microsoft.AspNet.SignalR;

namespace ChatApplication2.Hubs
{
    public class ChatHub : Hub<IChatClient>
    {
        public void PostMessage(string message)
        {
            message = HttpUtility.HtmlEncode(message);
            SendMessageDTO messageDto = new SendMessageDTO();
            using (ChatDbContext dbContext = new ChatDbContext())
            {
                var user = dbContext.ConnectedUsers.First(x => x.LastConnectionId == Context.ConnectionId);
                if (user != null)
                {
                    var newMessage = new Message()
                    {
                        Content = message,
                        Timestamp = DateTime.Now,
                        User = user
                    };

                    messageDto.MessageText = message;
                    messageDto.Username = user.Username;
                    messageDto.DateOfPost = newMessage.Timestamp;

                    dbContext.Messages.Add(newMessage);
                    dbContext.SaveChanges();
                }
            }

            Clients.AllExcept(Context.ConnectionId).BrodcastMessage(messageDto);
        }

        public void BrodcastUserConnected(string username, int userId)
        {
            UserLoggedInDTO loggedInDto = new UserLoggedInDTO()
            {
                LoginDate = DateTime.Now,
                Username = username,
                UserId = 0
            };
            using (ChatDbContext dbContext = new ChatDbContext())
            {
                var user = dbContext.ConnectedUsers.FirstOrDefault(x => x.Id == userId);
                if (user != null)
                {
                    loggedInDto.LoginDate = user.LastLoginDate;
                    loggedInDto.UserId = user.Id;
                    user.LastConnectionId = Context.ConnectionId;
                    dbContext.SaveChanges();
                }
            }
            Clients.AllExcept(Context.ConnectionId)
                .BrodcastNewUserConnected(loggedInDto);
        }

        public override Task OnConnected()
        {
            Debug.WriteLine("User connected", Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            using (ChatDbContext context = new ChatDbContext())
            {
                var user = context.ConnectedUsers
                    .FirstOrDefault(x => x.LastConnectionId == Context.ConnectionId);
                if (user != null)
                {
                    user.LoggedIn = false;

                    context.SaveChanges();

                    Clients.Others.BrodcastUserDisconnected(user.Id);
                }
            }
            return base.OnDisconnected(stopCalled);
        }
    }

    public interface IChatClient
    {
        /// <summary>
        /// Does a full message brodcast.
        /// </summary>
        /// <param name="message"></param>
        void BrodcastMessage(SendMessageDTO message);

        void BrodcastNewUserConnected(UserLoggedInDTO username);

        void BrodcastUserDisconnected(int userId);
    }
}