using ChatApplication2.Models.ChatModels;

namespace ChatApplication2.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ChatApplication2.Models.ChatModels.ChatDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ChatApplication2.Models.ChatModels.ChatDbContext context)
        {
            context.ConnectedUsers.AddOrUpdate(x=> x.Id, new User()
            {
                Id = 85,
                LastLoginDate = DateTime.Now,
                LoggedIn = true,
                Username = "lol"
            });

            context.SaveChanges();
        }
    }
}
