namespace ChatApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateUserTableAndMessage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "LastConnectionId", c => c.String());
            AddColumn("dbo.Messages", "User_Id", c => c.Int());
            CreateIndex("dbo.Messages", "User_Id");
            AddForeignKey("dbo.Messages", "User_Id", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "User_Id", "dbo.Users");
            DropIndex("dbo.Messages", new[] { "User_Id" });
            DropColumn("dbo.Messages", "User_Id");
            DropColumn("dbo.Users", "LastConnectionId");
        }
    }
}
