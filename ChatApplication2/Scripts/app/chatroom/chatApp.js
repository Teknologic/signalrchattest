﻿var chat = chat || {};

chat.initilize = function (chatHub, connection) {
    chat.MainVm = new chat.ChatMainViewModel(chatHub, connection);
};

chat.ChatMainViewModel = function(chatHub, connection) {
    var self = this;

    self.chatHub = chatHub;
    self.connection = connection;

    self.ConnectedUserViewModel = new chat.ConnectedUserViewModel(
        function () { return $.when(self.loadUsers(), self.loadLastHalfHourMessages()); },
        function () { },
        self.chatHub.server.brodcastUserConnected,
        self.connection);

    self.Messages = ko.observableArray([]);
    self.UsersVm = new chat.UsersViewModel();
    self.InputVm = new chat.InputViewModel(
        self.Messages,
        self.ConnectedUserViewModel,
        self.chatHub.server.postMessage);
    
    self.chatHub.client.brodcastNewUserConnected = function (data) {
        console.log("User with id of: ", data.UserId, " connected");
        self.UsersVm.Users.push(new chat.UserViewModel(data));
    };
    self.chatHub.client.brodcastMessage = function(data) {
        self.Messages.push(new chat.MessageViewModel(data));
    };
    self.chatHub.client.BrodcastUserDisconnected = function(id) {
        console.log("User with id of: ", id, " disconnected");
        self.UsersVm.removeUser(id);
    };
};

chat.InputViewModel = function(messages, user, sendMessageCallback) {
    var self = this;
    self.CurrentMessage = ko.observable("");
    self.Messages = messages;
    self.ConnectedUser = user;
    self.SendMessageCallback = sendMessageCallback;
};

chat.InputViewModel.prototype.SendMessage = function() {
    var self = this;

    if (self.CurrentMessage().length > 0) {
        self.SendMessageCallback(self.CurrentMessage());

        self.Messages.push(new chat.MessageViewModel({
            Username: self.ConnectedUser.WrapedUserName(),
            MessageText: self.CurrentMessage(),
            DateOfPost: new Date()
        }));
    }

    self.CurrentMessage("");
}
chat.ChatMainViewModel.prototype.loadUsers = function() {
    var self = this;
    return $.ajax({
        url: 'api/ChatApi/GetAllConnectedUsers',
        method: 'post',
        success: function(data) {
            data.forEach(function(item) {
                self.UsersVm.Users.push(new chat.UserViewModel(item));
            });
        }
    });
};
chat.ChatMainViewModel.prototype.loadLastHalfHourMessages = function() {
    var self = this;
    return $.ajax({
        url: 'api/ChatApi/Messages/Timespan/0/30',
        method: 'get',
        success: function(data) {
            data.forEach(function(item) {
                self.Messages.push(new chat.MessageViewModel(item));
            });
        }
    });
}

chat.UsersViewModel = function() {
    var self = this;
    self.Users = ko.observableArray([]);
    self.ConnectedUsers = ko.computed(function () {
        return self.Users().length;
    });
    self.removeUser = function(userId) {
        var elementIndex = self.Users().findIndex(function(element, index, array) {
            return element.UserId === userId;
        });

        if (elementIndex > -1) {
            self.Users.splice(elementIndex, 1);
        }
    };
}

chat.UserViewModel = function(data) {
    var self = this;
    self.Username = common.decodeHtmlEntities(data.Username);
    self.UserId = data.UserId;
};

chat.ConnectedUserViewModel = function(successLoginCallback, unsucefulLogin, userConnectedCallback, connection) {
    var self = this;
    self.CurrentUserName = ko.observable("");
    self.connection = connection;
    self.WrapedUserName = ko.computed({
        read: function() {
            return self.CurrentUserName();
        },
        write: function(value) {
            self.CurrentUserName(common.decodeHtmlEntities(value));
        },
        owner: this
    });

    self.successLoginCallback = successLoginCallback;
    self.unsucefulLogin = unsucefulLogin;
    self.LogedIn = ko.observable(false);
    self.LoadingLogin = ko.observable(false);

    self.UserLoginFormElement = $("#username-form-main");
    self.UserLoginFormElement.on('animationEnd webkitAnimationEnd oTransitionEnd oanimationend MSAnimationEndEnd',
        function () {
            self.UserLoginFormElement.removeClass("animate-wiggle");
        });

    self.UserConnectCallback = userConnectedCallback;
    self.UserId = 0;
};

chat.ConnectedUserViewModel.prototype.TryLogin = function() {
    var self = this;

    if (self.WrapedUserName().length <= 0) {
        self.LoadingLogin(false);
        self.UserLoginFormElement.addClass("animate-wiggle");
    } else {
        self.LoadingLogin(true);

        $.ajax({
            url: 'api/ChatApi/Login',
            method: 'post',
            data: { Username: self.WrapedUserName() },
            success: function(data) {
                self.UserId = data;
                
                self.successLoginCallback().done(function () {
                    self.connection.hub.start(function () {
                        self.UserConnectCallback(self.WrapedUserName(), self.UserId);
                        self.LoadingLogin(false);
                        self.LogedIn(true);
                    });
                });
            },
            error: function (error) {
                self.LogedIn(false);
                self.LoadingLogin(false);
                self.UserLoginFormElement.addClass("animate-wiggle");
            }
        });
    }
};

chat.MessageViewModel = function(data) {
    var self = this;
    self.MessageText = common.decodeHtmlEntities(data.MessageText);
    self.Username = common.decodeHtmlEntities(data.Username);
    self.DateOfPost = new Date(data.DateOfPost);

    self.Timestamp = ko.computed(function () {
        var hour = self.DateOfPost.getHours() < 10 ?  "0" + self.DateOfPost.getHours() : self.DateOfPost.getHours();
        var minutes = self.DateOfPost.getMinutes() < 10 ? "0" + self.DateOfPost.getMinutes() : self.DateOfPost.getMinutes();

        return hour + ":" + minutes;
    });
};