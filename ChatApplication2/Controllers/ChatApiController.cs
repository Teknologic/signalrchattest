﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using ChatApplication2.Models.ChatModels;
using ChatApplication2.Models.DTO;

namespace ChatApplication2.Controllers
{
    public class ChatApiController : ApiController
    {
        [HttpPost]
        public async Task<List<User>> GetAllConnectedUsers()
        {
            using (ChatDbContext context = new ChatDbContext())
            {
                var users = await context.ConnectedUsers.Where(x=>x.LoggedIn).ToListAsync();
                return users;
            }
        }

        [HttpGet]
        [Route("api/ChatApi/Messages/Timespan/{hours}/{minutes}")]
        public async Task<List<SendMessageDTO>> GetMessagesForHoursAndMinutes(int hours = 1, int minutes = 0)
        {
            // Sanitize input
            if (hours < 0 || minutes < 0)
                throw new HttpResponseException(HttpStatusCode.RequestedRangeNotSatisfiable);

            var upToDate = DateTime.Now - new TimeSpan(hours, minutes, 0);

            using (ChatDbContext context = new ChatDbContext())
            {
                var messages = await context.Messages.Where(x => x.Timestamp >= upToDate).ToListAsync();
                return messages.Select(x=> new SendMessageDTO()
                {
                    MessageText = x.Content,
                    Username = x.User.Username,
                    DateOfPost = x.Timestamp
                }).ToList();
            }
        }

        [HttpPost]
        public int Login(UserLoginDTO user)
        {
            using (ChatDbContext context = new ChatDbContext())
            {
                var existingUser = context.ConnectedUsers.FirstOrDefault(x => x.Username == user.Username);
                if (existingUser != null)
                {
                    if (existingUser.LoggedIn)
                    {
                        throw new HttpResponseException(HttpStatusCode.Unauthorized);
                    }

                    context.ConnectedUsers.Remove(existingUser);
                    context.SaveChanges();
                }

                var newUser = new User
                {
                    LastLoginDate = DateTime.Now,
                    LoggedIn = true,
                    Username = HttpUtility.HtmlEncode(user.Username)
                };

                var userDb = context.ConnectedUsers.Add(newUser);
                context.SaveChanges();

                return userDb.Id;
            }
        }
    }
}