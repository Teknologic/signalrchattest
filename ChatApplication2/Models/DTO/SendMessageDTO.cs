﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatApplication2.Models.DTO
{
    public class SendMessageDTO
    {
        public string Username { get; set; }
        
        public string MessageText { get; set; }

        public DateTime DateOfPost { get; set; }

        public string TimePassedUntilNowAndDateOfPost { get; set; }
    }
}