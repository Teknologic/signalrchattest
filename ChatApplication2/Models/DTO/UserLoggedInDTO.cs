﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace ChatApplication2.Models.DTO
{
    public class UserLoggedInDTO
    {
        public int UserId { get; set; }
        
        public string Username { get; set; }
        
        public DateTime LoginDate { get; set; }
    }
}