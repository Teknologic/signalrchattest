﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatApplication2.Models.DTO
{
    public class UserLoginDTO
    {
        public string Username { get; set; }
    }
}