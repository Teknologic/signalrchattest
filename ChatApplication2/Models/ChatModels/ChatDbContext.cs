﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ChatApplication2.Models.ChatModels
{
    public class ChatDbContext : DbContext
    {
        public ChatDbContext() : base("ChatDb")
        {
        }

        public DbSet<ChatModels.User> ConnectedUsers { get; set; }
        public DbSet<ChatModels.Message> Messages { get; set; }

    }
}