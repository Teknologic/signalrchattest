﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatApplication2.Models.ChatModels
{
    public class Message
    {
        public int Id { get; set; }
        
        public string Content { get; set; }

        public DateTime Timestamp { get; set; }

        public virtual User User { get; set; }
    }
}