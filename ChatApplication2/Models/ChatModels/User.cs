﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SqlServer.Server;

namespace ChatApplication2.Models.ChatModels
{
    public class User
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public bool LoggedIn { get; set; }

        public DateTime LastLoginDate { get; set; }

        public string LastConnectionId { get; set; }
    }
}