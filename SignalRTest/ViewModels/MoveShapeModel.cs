﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SignalRTest.Models
{
    public class MoveShapeModel
    {
        [JsonProperty("left")]
        public float Left { get; set; }

        [JsonProperty("top")]
        public float Top { get; set; }

        [JsonIgnore]
        public string LastUpdatedBy { get; set; }
    }
}