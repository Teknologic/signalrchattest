﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SignalRTest
{
    public class ChatHub : Hub
    {
        public void SendMessage(string name, string message)
        {
            Clients.All.brodcastMessage(name, message);
        }

        public void StartTyping(string name)
        {
            Clients.All.brodcastUserTyping(name);
        }

        public void StoppedTyping(string name)
        {
            Clients.All.brodcastUserStoppedTyping(name);
        }
    }
}