﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using SignalRTest.Models;

namespace SignalRTest
{
    public class Brodcaster
    {
        private Timer internalBrodcastTimer;
        private MoveShapeModel _shapeModel;
        private bool _modelUpdated = false;
        private MoveShapeHub _hub;

        public Brodcaster(MoveShapeModel shapeModel, MoveShapeHub hub)
        {
            _hub = hub;

            var messagePerSecond = 10;
            var timerInterval = 1000 / messagePerSecond;

             internalBrodcastTimer = new Timer(BrodcastUpdate, null, timerInterval, timerInterval);
        }

        private void BrodcastUpdate(object state)
        {
            if (_modelUpdated)
            {
                _hub.Clients.AllExcept(_shapeModel.LastUpdatedBy).updateShapeForClient(_shapeModel);
                _modelUpdated = false;
            }
        }

        public void UpdateShape(MoveShapeModel model)
        {
            _modelUpdated = true;
            _shapeModel = model;
        }
    }



    public class MoveShapeHub : Hub
    {
        private static Dictionary<string, string> users = new Dictionary<string, string>();
        private Brodcaster brodcaster;

        public MoveShapeHub()
        {
            brodcaster = new Brodcaster(new MoveShapeModel(), this);
        }

        public void UpdateModel(MoveShapeModel model)
        {
            model.LastUpdatedBy = Context.ConnectionId;
            brodcaster.UpdateShape(model);
        }

        public void NewListener(string username)
        {
            var message = string.Format("{0} has joined", username);
            users.Add(Context.ConnectionId, username);
            Clients.AllExcept(Context.ConnectionId).userConnectedNotification(message, Context.ConnectionId);
        }
        
        public override Task OnDisconnected(bool stopCalled)
        {
            users.Remove(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }
    }
}